import Graphics.Gloss

--The main body
main :: IO()
main = animate (InWindow "Gloss" (1000,1000) (0,0)) black picture

--Circle Size
size :: Float
size = 500

--Picture to draw
--f denotes time since start
picture :: Float -> Picture
picture f = drawCartoid (f+1)

--Set of Integral Points along the circle
--used for drawing the circle and labelling the points
circlePoints :: Float -> [Point]
circlePoints n = cycle [ (sin (2*pi*k/n), cos (2*pi*k/n)) | k <- [0..(n'-1)] ]
  where n' = fromIntegral $ floor n

--Zip points along the circle with a label
labeledCirclePoints :: Float -> [(Int,Point)]
labeledCirclePoints r = zip [0..] $ circlePoints r

--get point on the circle (n = number of points, k = the point you want)
--used to draw the lightbeam in a Continuous way
circlePointAsAFunction :: Float -> Float -> Point
circlePointAsAFunction n k = (sin (2*pi*k/n), cos (2*pi*k/n))

--draw line based on multiplicatio
drawLightBeams :: Float -> Float -> Float -> Picture
drawLightBeams c t n = Line [(points n), (points (c*n))]
  where points = circlePointAsAFunction t

--draw everything (cartoid-thingy light beams)
drawCartoid :: Float -> Picture
drawCartoid f = scale size size . color white $ pictures [
  line [(10,10),(10,-10),(-10,-10),(-10,10)] -- here for debugging purposes
  --Draws the circle
  , line . map snd . take (n'+1) $ labeledCirclePoints n
  --shows the label of each point
  , scale (1/size) (1/size) . pictures . map (\(label,(x,y)) -> (scale 0.2 0.2).(translate (5.2*x*size - 50) (5.2*y*size - 50)).text.show $ label) . take n' $ labeledCirclePoints n
  --draws both the lightbeams and the reflected lightbeams (the main show)
  , pictures . map (drawLightBeams c n) $ [0..n]
  --text denote current scalefactor (displayed right-top)
  , translate 0.75 0.85 . scale (0.5/size) (0.5/size) . text $ show c
  ]
    -- constants that influence number of points, speed of animation etc
    where c = f/4 --2
          n = 400 --number of points
          n' = floor n

